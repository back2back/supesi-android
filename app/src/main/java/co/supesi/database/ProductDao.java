package co.supesi.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import co.supesi.model.Product;
import co.supesi.model.ProductAndStock;
import co.supesi.model.StoreProduct;

@Dao
public interface ProductDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Product products);

    @Query("SELECT * from products ORDER BY title COLLATE NOCASE ASC")
    LiveData<List<Product>> getProducts();


    @Transaction
    @Query("SELECT * from products ORDER BY title DESC")
    LiveData<List<ProductAndStock>> getProductsWithStocks();

    @Query("DELETE from products")
    void deleteAll();

    @Query("SELECT products.* FROM products JOIN productsFts ON (products.id = productsFts.rowid) "
            + "WHERE productsFts MATCH :query")
    LiveData<List<Product>> searchAllProducts(String query);

}
