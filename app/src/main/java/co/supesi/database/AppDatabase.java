package co.supesi.database;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import co.supesi.AppExecutors;
import co.supesi.model.Customer;
import co.supesi.model.Order;
import co.supesi.model.Product;
import co.supesi.model.ProductFts;
import co.supesi.model.Stock;
import co.supesi.model.Team;
import co.supesi.model.Tenant;
import co.supesi.model.User;

@Database(entities = {
        Product.class,
        ProductFts.class,
        Order.class,
        User.class,
        Tenant.class,
        Team.class,
        Stock.class,
        Customer.class
}, version = 37)
public abstract class AppDatabase extends RoomDatabase {

    @VisibleForTesting
    public static final String DATABASE_NAME = "supesi_database";

    public abstract ProductDao productDao();
    public abstract OrderDao orderDao();
    public abstract UserDao userDao();
    public abstract TenantDao tenantDao();
    public abstract TeamDao teamDao();
    public abstract StockDao stockDao();
    public abstract CustomerDao customerDao();
    private static AppDatabase INSTANCE;

    public static AppDatabase getInstance(Context context, AppExecutors executors){
        if (INSTANCE == null){
            synchronized (AppDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),AppDatabase.class, DATABASE_NAME)
                            .addMigrations(MIGRATION_36_37)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static final Migration MIGRATION_36_37 = new Migration(36,37) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE VIRTUAL TABLE IF NOT EXISTS `productsFts` USING FTS4("
                    + "`title` TEXT, `description` TEXT, content=`products`)");
            database.execSQL("INSERT INTO productsFts (`rowid`, `title`, `description`) "
                    + "SELECT `id`, `title`, `description` FROM products");
        }
    };

}
