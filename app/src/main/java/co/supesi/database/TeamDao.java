package co.supesi.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import co.supesi.model.Team;


@Dao
public interface TeamDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Team team);

    @Query("SELECT * FROM teams ")
    LiveData<List<Team>> getTeam();

    @Query("DELETE from teams")
    void deleteAll();
}
