package co.supesi.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import co.supesi.model.Customer;

@Dao
public interface CustomerDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Customer ...customers);

    @Query("SELECT * from customers")
    LiveData<List<Customer>> getCustomers();

    //get the customer
    @Query("SELECT * from customers WHERE id =:id")
    LiveData<Customer> getOneCustomer(int id);
}
