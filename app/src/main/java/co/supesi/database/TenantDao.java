package co.supesi.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import co.supesi.model.Tenant;
import co.supesi.model.User;

@Dao
public interface TenantDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Tenant tenant);

    @Query("SELECT * FROM tenants ")
    LiveData<List<Tenant>> getTenant();

    @Query("DELETE from tenants")
    void deleteAll();
}
