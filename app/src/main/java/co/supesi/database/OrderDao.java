package co.supesi.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import co.supesi.model.Order;

@Dao
public interface  OrderDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
     void insert(Order...orders);

    @Query("SELECT * from orders")
     LiveData<List<Order>> getOrders();

    @Query("DELETE from orders")
     void deleteAll();

    @Query("DELETE from orders WHERE product_id =:productId")
     void deleteOneOrder(int productId);

    @Query("UPDATE orders SET qty =:quantitySet WHERE product_id =:productId")
     void  getUpdatedOrder(int productId, int quantitySet);

    //gets the total amount of order items
    @Query("SELECT SUM(qty * sell_at) FROM ORDERS")
     LiveData <Double> getTotals();


    //assume all orders one customer
    @Query("UPDATE orders SET customerId =:customerId ")
    void updateCustomerOrder(int customerId);

    @Query("SELECT * FROM orders WHERE product_id =:productId")
    Order findByProductId(int productId);




}
