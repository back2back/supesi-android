package co.supesi.database;

import co.supesi.model.Customer;

public interface OnCustomerClickListener {
    void onCustomerClick(Customer customer);
}
