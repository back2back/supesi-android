package co.supesi.database;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import co.supesi.AppExecutors;
import co.supesi.BasicApp;
import co.supesi.model.Customer;
import co.supesi.model.Order;
import co.supesi.model.Product;
import co.supesi.model.ProductAndStock;
import co.supesi.model.Stock;
import co.supesi.model.Team;
import co.supesi.model.Tenant;
import co.supesi.model.User;
import co.supesi.utility.Constants;

public class DataRepository {

    private static DataRepository sInstance;
    static AppExecutors appExecutors;

    private static final String TAG = "DataRepository";
    String token;
    SharedPreferences pref;
    ProductDao productDao;
    LiveData<List<Product>> allProducts;

    LiveData<List<ProductAndStock>> allProductsAndStock;


    OrderDao orderDao;
    LiveData<List<Order>> allOrders;
    Order orderByProductId;
    LiveData<Double> totals;
    LiveData<Integer> count;

    //todo mutable livedata observe
    MutableLiveData<Double> customCount;

    UserDao userDao;
    LiveData<List<User>> user;

    TenantDao tenantDao;
    LiveData<List<Tenant>> tenant;

    TeamDao teamDao;
    LiveData<List<Team>> teams;

    StockDao stockDao;
    LiveData<List<Stock>> stocks;

    CustomerDao customerDao;
    LiveData<List<Customer>> customers;

    AppDatabase db;

    private DataRepository(final  AppDatabase appDatabase){
        db = appDatabase;
        productDao = db.productDao();
        allProducts = productDao.getProducts();
        allProductsAndStock = productDao.getProductsWithStocks();

        orderDao = db.orderDao();
        allOrders = orderDao.getOrders();
        totals = orderDao.getTotals();


        userDao = db.userDao();
        user = userDao.getUser();

        tenantDao = db.tenantDao();
        tenant = tenantDao.getTenant();

        teamDao = db.teamDao();
        teams = teamDao.getTeam();

        stockDao = db.stockDao();
        stocks = stockDao.getStocks();

        customerDao = db.customerDao();
        customers = customerDao.getCustomers();

    }

    public static DataRepository getInstance(final AppDatabase database, AppExecutors executors) {
        if (sInstance == null) {
            synchronized (DataRepository.class) {
                if (sInstance == null) {
                    sInstance = new DataRepository(database);
                    appExecutors = executors;
                }
            }
        }
        return sInstance;
    }

    public String getToken(){
        return token;
    }
    public  LiveData<Stock> mine(int id){
        return stockDao.getCount(id);
    }

    //read operations
    public LiveData<List<Product>> getAllProducts(){

        return  allProducts;
    }

    public LiveData<List<Product>> searchProducts(String query){
        return  productDao.searchAllProducts(query);
    }



    // all orders
    public LiveData<List<Order>> getAllOrders(){
        return allOrders;
    }

    //orders by product id
    public void getOrderByProductId(Order order){

        appExecutors.diskIO().execute(()-> {
            orderByProductId = orderDao.findByProductId(order.getProduct_id());
            if (orderByProductId != null){
                //get prev qty
                int prevQty;
                prevQty = orderByProductId.getQuantity();
                orderDao.getUpdatedOrder(orderByProductId.getProduct_id(),1 + prevQty);
            }else{
                orderDao.insert(order);
            }
        });

    }
    public LiveData<Integer> getCount(){
        return count;
    }

    public LiveData<Double> getTotals(){
        return totals;
    }


    public LiveData<List<User>> getUser (){ return  user;}

    public LiveData<List<Tenant>> getTenant (){
        return tenant;
    }

    public LiveData<List<Team>> getTeams (){return  teams;}

    public LiveData<List<Stock>> getStocks(){return stocks;}

    //list of customers
    public LiveData<List<Customer>> getCustomers(){return  customers;}

    //customer by id
    public LiveData<Customer> getCustomerById(int customerId){
        return customerDao.getOneCustomer(customerId);
    }

    //fooz
    public LiveData<List<ProductAndStock>> withStocks(){
        return allProductsAndStock;
    }

    public void insertCustomer(Customer customer){

        appExecutors.diskIO().execute(() ->{
            customerDao.insert(customer);
        });
    }



    //write operations
    public void insertProduct(Product products){
        appExecutors.diskIO().execute(()-> {
            productDao.insert(products);

            //should we update stocks as well?
            //stockDao.insert(products.getStock());
            insertStock(products.getStock());


        });
    }

    public void insertStock(Stock stock){
        appExecutors.diskIO().execute(()-> {
            stockDao.insert(stock);
        });
    }
    public void insertOrder(Order order){
        appExecutors.diskIO().execute(()-> {
            orderDao.insert(order);
        });

    }

    public void updateQuantity(Order order){
        appExecutors.diskIO().execute(() -> {
            orderDao.getUpdatedOrder(order.getProduct_id(),order.getQuantity());
        });
    }

    public void updateCustomerOrder(Customer customer){
        appExecutors.diskIO().execute(() -> {
            orderDao.updateCustomerOrder(customer.getId());
        });
    }
    public void deleteAllOrders(){
        appExecutors.diskIO().execute(()-> {
            orderDao.deleteAll();
        });
    }

    public void deleteSingleOrder(Order order){
        appExecutors.diskIO().execute( ()->{
            orderDao.deleteOneOrder(order.getProduct_id());
        } );
    }

    public void insertUser(User user, Tenant tenant){
        appExecutors.diskIO().execute(()-> {
            userDao.insert(user);
            tenantDao.insert(tenant);

            ArrayList<Team> teams = tenant.getTeams();

            for (Team team: teams){
                Team team1 = new Team();
                team1.setOrgRole(team.getOrgRole());
                team1.setUserId(team.getUserId());

                User user1 = team.getUser();
                team1.setId(team.getId());
                team1.setFirstName(user1.getFirstName());
                team1.setTenantId(team.getTenantId());
                teamDao.insert(team1);

            }

        });
    }


    public void nukeTables(){
        //remove some tables
        appExecutors.diskIO().execute(() -> {
            orderDao.deleteAll();
            productDao.deleteAll();
            //teamDao.deleteAll();
            //tenantDao.deleteAll();
            //userDao.deleteAll();
        });

    }



}
