package co.supesi.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import co.supesi.model.Product;
import co.supesi.model.Stock;

@Dao
public interface StockDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Stock stocks);

    @Query("SELECT * FROM stocks")
    LiveData<List<Stock>> getStocks();

    @Query("SELECT * FROM stocks WHERE productId =:productId")
    LiveData<Stock>getCount(int productId);

    //update
    @Update
    void update(Stock stock);

    @Query("DELETE from stocks")
    void deleteAll();
}

