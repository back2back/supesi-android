package co.supesi;

import android.app.Application;

import com.facebook.stetho.Stetho;

import co.supesi.database.AppDatabase;
import co.supesi.database.DataRepository;
import im.crisp.sdk.Crisp;

public class BasicApp extends Application {

    private static BasicApp mContext;
    private AppExecutors mAppExecutors;
    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

        mAppExecutors = new AppExecutors();
        Crisp.initialize(this);
        // Replace it with your WEBSITE_ID
        // Retrieve it using https://app.crisp.chat/website/[YOUR_WEBSITE_ID]/
        Crisp.getInstance().setWebsiteId(getString(R.string.crisp_id));

    }

    public static BasicApp getContext(){
        return mContext;
    }
    public AppDatabase getDatabase() {
        return AppDatabase.getInstance(this, mAppExecutors);
    }

    public DataRepository getRepository() {
        return DataRepository.getInstance(getDatabase(), mAppExecutors);
    }
}
