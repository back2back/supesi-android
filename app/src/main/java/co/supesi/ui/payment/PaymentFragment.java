package co.supesi.ui.payment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.supesi.BasicApp;
import co.supesi.R;
import co.supesi.model.ApiOrderResponse;
import co.supesi.model.Customer;
import co.supesi.model.Order;
import co.supesi.model.Team;
import co.supesi.network.GetData;
import co.supesi.network.RetrofitClientInstance;
import co.supesi.ui.cart.ProductsCartViewModel;
import co.supesi.ui.customer.CustomerFragment;
import co.supesi.ui.customer.CustomerViewModel;
import co.supesi.utility.Constants;
import co.supesi.utility.FontManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PaymentFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private static final String TAG = "PaymentFragment";

    private int userId;

    private PaymentViewModel paymentViewModel;
    CustomerViewModel customerViewModel;
    private Unbinder unbinder;

    @BindView(R.id.ib_cash)
    ImageButton btnCash;
    @BindView(R.id.ib_mpesa)
    ImageButton btnMpesa;
    @BindView(R.id.tv_total)
    TextView tvTotal;
    @BindView(R.id.tv_customer_name)
            TextView tvCustomer;
    @BindView(R.id.tv_customer_phone)
            TextView tvCustomerPhone;

    NavController navController;
    ArrayAdapter<Team > adapter;
    String token;
    SharedPreferences sharedPref;
    private ProductsCartViewModel productsCartViewModel;

    public PaymentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(view);
        sharedPref = requireActivity().getSharedPreferences(Constants.getPrefJwt(), Context.MODE_PRIVATE);


        token = sharedPref.getString("jwt",null);
        paymentViewModel =
                new ViewModelProvider(requireActivity()).get(PaymentViewModel.class);
        productsCartViewModel =
                new ViewModelProvider(requireActivity()).get(ProductsCartViewModel.class);

        customerViewModel =
                new ViewModelProvider(requireActivity()).get(CustomerViewModel.class);
        adapter = new ArrayAdapter<>(requireActivity(),  android.R.layout.simple_spinner_item);
        paymentViewModel.getTeams().observe(getViewLifecycleOwner(), new Observer<List<Team>>() {
            @Override
            public void onChanged(List<Team> teams) {
                for (Team t: teams){
                    adapter.add(t);
                }
            }
        });


        productsCartViewModel.getTotals().observe(getViewLifecycleOwner(), new Observer<Double>() {
            @Override
            public void onChanged(Double aFloat) {
                tvTotal.setText(String.valueOf(aFloat));
            }
        });

        productsCartViewModel.getOrdersFromDb().observe(getViewLifecycleOwner(), orders -> {

                if (orders.size() ==0){
                    return;
                }
                Order o = orders.get(0);


                //get the customer id
                if (o.getCustomerId() == 0){

                    return;
                }
                customerViewModel
                        .getCustomerById(o.getCustomerId())
                        .observe(getViewLifecycleOwner(),
                                customer -> {
                            tvCustomer.setText(customer.getUsername());
                            tvCustomerPhone.setText(String.valueOf(customer.getPhone()));
                        }
                        );

        });



        tvCustomer.setOnClickListener(view12 -> {
            //launch the customer fragment
            Navigation.findNavController(view12).navigate(R.id.action_nav_payment_to_customer);
        });

        tvCustomerPhone.setOnClickListener(view1 -> {
            //launch the customer fragment
            Navigation.findNavController(view1).navigate(R.id.action_nav_payment_to_customer);
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Team team =(Team)  parent.getItemAtPosition(position);
        userId = team.getUserId();


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @OnClick({R.id.ib_cash})
    public void cash(){

        postOrder(0);

    }

    @OnClick({R.id.ib_mpesa})
    public void mpesa(){

        postOrder(1);

    }
    private void returnHome(){
        navController.popBackStack(R.id.nav_home,false);
    }
    public void postOrder(int paymentType){

        List<Order> orderList = productsCartViewModel.getOrdersFromDb().getValue();

        assert orderList != null;
        for(Order foo: orderList){
            foo.setPreparedBy(userId);
            foo.setPaymentType(paymentType);
        }
        //clear the orders db
        //go back to home screen
        GetData getData = RetrofitClientInstance.getRetrofitInstance().create(GetData.class);
        Call<ApiOrderResponse> createOrder = getData.addOrders(token, (ArrayList<Order>) orderList);
        createOrder.enqueue(new Callback<ApiOrderResponse>() {
            @Override
            public void onResponse(Call<ApiOrderResponse> call, Response<ApiOrderResponse> response) {
                if (response.body() != null){
                    ApiOrderResponse apiOrderResponse = response.body();
                    boolean msg = Boolean.parseBoolean(apiOrderResponse.getMessage());
                    if (msg){
                        Toast.makeText(getContext(), "Sent to server", Toast.LENGTH_SHORT).show();
                        productsCartViewModel.deleteOrders();
                        returnHome();
                    }else{
                        Toast.makeText(getContext(), "Received but error", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getContext(), "Cannot makes order", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiOrderResponse> call, Throwable t) {
                Toast.makeText(getContext(), "Error failed", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
