package co.supesi.ui.payment;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import co.supesi.BasicApp;
import co.supesi.database.DataRepository;
import co.supesi.model.Order;
import co.supesi.model.Team;

public class PaymentViewModel extends AndroidViewModel {

    private static final String TAG = "PaymentViewModel";
    private DataRepository dataRepository;
    private LiveData<List<Team>> teams;
    public PaymentViewModel(@NonNull Application application) {
        super(application);
        dataRepository = BasicApp.getContext().getRepository();
        teams = dataRepository.getTeams();
    }

    public LiveData<List<Team>> getTeams(){
        return teams;
    }
}
