package co.supesi.ui;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import co.supesi.database.DataRepository;
import co.supesi.model.ApiUserResponse;
import co.supesi.model.Tenant;
import co.supesi.model.User;
import co.supesi.network.GetData;
import co.supesi.network.RetrofitClientInstance;
import co.supesi.utility.Constants;
import co.supesi.utility.SaveUser;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends AndroidViewModel {

    public static final String TAG = LoginViewModel.class.getSimpleName();


    public enum AuthenticationState{
        UNAUTHENTICATED,
        AUTHENTICATED,
        INVALID_AUTHENTICATION
    }

    String jwt;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    boolean result;
    private DataRepository dataRepository;


    final MutableLiveData<AuthenticationState> authState = new MutableLiveData<>();

    public LoginViewModel(Application application){
        super(application);


        preferences = getApplication().getSharedPreferences(Constants.getPrefJwt(), Context.MODE_PRIVATE);
        jwt = preferences.getString("jwt",null);


        if (jwt == null){
            authState.setValue(AuthenticationState.UNAUTHENTICATED);

        }else{
            authState.setValue(AuthenticationState.AUTHENTICATED);

        }



    }

    public void authenticateOnline(String email, String password){
        isLoginValid(email,password);
    }

    public MutableLiveData<AuthenticationState> getAuthState(){
        return authState;
    }

    public void refuseAuthentication() {
        authState.setValue(AuthenticationState.UNAUTHENTICATED);
    }

    private void writePref(String jwt){
        preferences = getApplication().getSharedPreferences(Constants.getPrefJwt(), Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString("jwt", jwt);
        editor.apply();

    }

    private void isLoginValid(String email, String password){
        //check the server

        GetData getData = RetrofitClientInstance.getRetrofitInstance().create(GetData.class);
        Call<ApiUserResponse> loginCall = getData.login(new User(email, password));
        loginCall.enqueue(new Callback<ApiUserResponse>() {
            @Override
            public void onResponse(Call<ApiUserResponse> call, Response<ApiUserResponse> response) {
                if(response.body()!= null){
                    ApiUserResponse apiUserResponse = response.body();
                    boolean msg = Boolean.parseBoolean(apiUserResponse.getMessage());

                    if (msg){
                        //write to shared pref
                        writePref(apiUserResponse.getJwt());
                        LoginViewModel.this.authState.setValue(AuthenticationState.AUTHENTICATED);

                        new SaveUser(getApplication()).saveUserData(apiUserResponse);

                    }else{
                        LoginViewModel.this.authState.setValue(AuthenticationState.INVALID_AUTHENTICATION);

                    }
                }
            }

            @Override
            public void onFailure(Call<ApiUserResponse> call, Throwable t) {

                LoginViewModel.this.authState.setValue(AuthenticationState.INVALID_AUTHENTICATION);
            }
        });

    }


}
