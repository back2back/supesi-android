package co.supesi.ui.editproduct;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.R;
import co.supesi.model.ApiStoreProductResponse;
import co.supesi.model.Product;
import co.supesi.model.Stock;
import co.supesi.network.GetData;
import co.supesi.network.RetrofitClientInstance;
import co.supesi.ui.createproduct.CreateProduct;
import co.supesi.utility.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProduct extends Fragment {
    @BindView(R.id.et_price)
    EditText etSellingPrice;
    @BindView(R.id.et_prod_name_create)
    EditText etProductName;
    @BindView(R.id.et_prod_description)
    EditText etDescription;
    @BindView(R.id.btn_prod_update)
    Button btnUpdateProduct;
    @BindView(R.id.tv_stock)
    TextView tvStock;
    @BindView(R.id.tv_adjust_stock)
    TextView tvAdjustStock;

    @BindView(R.id.tv_lbl_amt)
    TextView foo;

    ProgressDialog progressDialog;

    String productName;
    String sellingPrice;
    String description;
    //String stockCount;

    SharedPreferences sharedPref;
    String token;

    private final static String TAG = "EditProduct";

    private Unbinder unbinder;

    Serializable fromBundle, stockAlso;
    String args;
    public EditProduct() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.frag_edit_product, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        assert getArguments() != null;
        fromBundle = getArguments().getSerializable("amount");
        stockAlso = getArguments().getSerializable("stockPojo");
        args = getArguments().getString("stock");


        Product test = (Product)fromBundle;
        foo.setText(test.getTitle());
        token = getToken(view);

        Stock stock = (Stock)stockAlso;

        //set the views
        etProductName.setText(test.getTitle());
        etSellingPrice.setText(test.getSellingPrice().toString());
        etDescription.setText(test.getDescription());
        tvStock.setText(args);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Editing product");
        btnUpdateProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog.show();
                //get the data
                productName = etProductName.getText().toString();
                sellingPrice = etSellingPrice.getText().toString();
                description = etDescription.getText().toString();
                //stockCount = etStock.getText().toString();

                /*if (stockCount.equals("")){
                    stockCount = "0";
                }*/
                if (sellingPrice == null){
                    Toast.makeText(requireActivity(), "Must set price", Toast.LENGTH_SHORT).show();
                }else{

                    float sp = Float.parseFloat(sellingPrice);
                    //int stk = Integer.parseInt(stockCount);

                    editProduct(productName, sp, description, 9, test);
                    etProductName.getText().clear();
                    etSellingPrice.getText().clear();
                    etDescription.getText().clear();
                    //etStock.getText().clear();
                }

            }
        });

        tvAdjustStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Go to reasons fragment
                Bundle bundle = new Bundle();
                bundle.putSerializable("stockPojo", stock);

                Navigation.findNavController(v).navigate(R.id.action_nav_edit_product_to_pick_reason, bundle);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void editProduct(String productName, Float sellingPrice, String description, int stock, Product test){

        Product product = new Product(productName, sellingPrice, 0f,description);
        GetData productWhat = RetrofitClientInstance.getRetrofitInstance().create(GetData.class);
        Call<ApiStoreProductResponse> listCall = productWhat.editProduct(token, test.getId(), product);

        listCall.enqueue(new Callback<ApiStoreProductResponse>() {
            @Override
            public void onResponse(Call<ApiStoreProductResponse> call, Response<ApiStoreProductResponse> response) {


                //notify user of success
                if (response.body() != null){
                    progressDialog.dismiss();

                    ApiStoreProductResponse apiStoreProductResponse = response.body();
                    boolean msg = (Boolean.parseBoolean(apiStoreProductResponse.getMessage()));
                    if (msg){
                        Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getActivity(), "Error editing product", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiStoreProductResponse> call, Throwable t) {

                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Failure networkd data", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private String getToken(View view){
        sharedPref = view.getContext().getSharedPreferences(Constants.getPrefJwt(), Context.MODE_PRIVATE);
        token = sharedPref.getString("jwt",null);
        if (token !=null){
            //fetchOnline();
            return  token;
        }
        return "no-token";
    }
}
