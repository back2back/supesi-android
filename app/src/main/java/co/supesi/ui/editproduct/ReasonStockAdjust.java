package co.supesi.ui.editproduct;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.R;
import co.supesi.model.Stock;


public class ReasonStockAdjust extends Fragment implements AdapterView.OnItemClickListener {

    private final String TAG = "StockAdjustment";
    private Unbinder unbinder;
    @BindView(R.id.lv_reasons)
    ListView listView;
    private Serializable args;
    Stock stock;

    public ReasonStockAdjust() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        args = getArguments().getSerializable("stockPojo");
        stock = (Stock) args;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_reason_adjustment, container, false);
        unbinder = ButterKnife.bind(this, view);
        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Resources res = getResources();
        String[] reasons = res.getStringArray(R.array.restock_reasons);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(requireActivity(),
                android.R.layout.simple_list_item_1, reasons);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


        Bundle bundle = new Bundle();
        bundle.putLong("reason", id);
        bundle.putSerializable("stockPojo", args);
        // TODO assert order is correct then go to new fragment to adjust number
        Navigation.findNavController(view).navigate(R.id.action_pick_reason_to_adjust_stock, bundle);
    }
}