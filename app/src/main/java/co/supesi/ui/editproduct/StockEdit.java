package co.supesi.ui.editproduct;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.R;
import co.supesi.model.ApiStockRetake;
import co.supesi.model.Stock;
import co.supesi.model.StockRetake;
import co.supesi.network.GetData;
import co.supesi.network.RetrofitClientInstance;
import co.supesi.utility.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;


public class StockEdit extends Fragment {

    private Unbinder unbinder;

    Long mParam1;
    Serializable stockPojo;
    Stock stock;
    @BindView(R.id.btn_stock_retake)
    Button button;
    @BindView(R.id.et_stock_qty)
    EditText etStock;
    @BindView(R.id.tv_old_stock)
    TextView tvOldStock;
    @BindView(R.id.tv_new_stock)
    TextView tvNewStock;
    @BindView(R.id.tv_lbl_amt)
    TextView tvLblQty;
    @BindView(R.id.et_comment)
            EditText etComment;

    SharedPreferences sharedPref;
    String token;

    public StockEdit() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getLong("reason");
            stockPojo = getArguments().getSerializable("stockPojo");

        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_stock_edit, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //pop back to edit product overview

                whatsin();
            }
        });

        hideKeyboard();
        calculate();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void hideKeyboard(){
        etStock.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    v.clearFocus();
                    InputMethodManager in = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(v.getWindowToken(), 0);


                }
                return true;
            }
        });
    }

    private void calculate() {
        stock = (Stock) stockPojo;
        tvOldStock.setText(String.valueOf(stock.getCounted()));
        long cc = mParam1;
        int x = (int) cc;
        switch (x){
            case 0:
                //add stock
                getReason(0);
                break;
            case 1:
                getReason(1);
                //recount discard what was there
                break;
            case 2:
                //theft sub
                getReason(2);
                break;
        }
    }

    private void getReason(int d){
        //as user types, get the value add it to what we have
        Resources resources = getResources();
        String [] reasons = resources.getStringArray(R.array.restock_reasons);
        tvLblQty.setText(reasons[d]);

        updateView(d);

    }

    private void whatsin(){
        String ff = etStock.getText().toString();
        String commz = etComment.getText().toString();
        stock = (Stock) stockPojo;
        //Toast.makeText(requireActivity(), ff, Toast.LENGTH_SHORT).show();
        int qty = Integer.parseInt(ff);
        long cc = mParam1;
        int reasoning = (int) cc;



        sharedPref = requireActivity().getSharedPreferences(Constants.getPrefJwt(),MODE_PRIVATE);
        token = sharedPref.getString("jwt", null);

        StockRetake stockRetake = new StockRetake();
        stockRetake.setReason(reasoning);
        stockRetake.setComment(commz);
        stockRetake.setStockId(stock.getIdStock());
        stockRetake.setQty(qty);
        //start network
        GetData getData = RetrofitClientInstance.getRetrofitInstance().create(GetData.class);
        Call<ApiStockRetake> call = getData.addStockRetake(token, stockRetake);
        call.enqueue(new Callback<ApiStockRetake>() {
            @Override
            public void onResponse(Call<ApiStockRetake> call, Response<ApiStockRetake> response) {
                if (response.body() != null){
                    process(response.body());
                }
            }

            @Override
            public void onFailure(Call<ApiStockRetake> call, Throwable t) {

            }
        });
    }

    private void process(ApiStockRetake body) {
        boolean msg = Boolean.parseBoolean(body.getMessage());
        if (msg){
            // we successful add stock retake;
            //Navigation.findNavController(v).navigate(R.id.action_back_to_edit);

        }
    }

    private void updateView(int d) {
        stock = (Stock) stockPojo;
        etStock.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                   String j = s.toString();
                   if (j.isEmpty()){
                       tvNewStock.setText("0");
                   }else{
                       switch (d){
                           case 0:
                               int k = Integer.parseInt(j);
                               int val = stock.getCounted() + k;
                               tvNewStock.setText(String.valueOf(val));
                               break;
                           case  1:
                               int y = Integer.parseInt(j);
                               tvNewStock.setText(String.valueOf(y));
                               break;

                           case 2:
                               int z = Integer.parseInt(j);
                               int valb = stock.getCounted() - z;
                               if (valb < 0){
                                   valb = 0;
                                   tvNewStock.setText("0");
                               }else{
                                   tvNewStock.setText(String.valueOf(valb));
                               }

                               break;
                       }

                   }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


}