package co.supesi.ui.onlineorders;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.R;
import co.supesi.adapter.OnlineOrdersAdapter;
import co.supesi.model.ApiReadyOrderResponse;
import co.supesi.model.OrderEntry;
import co.supesi.network.GetData;
import co.supesi.network.RetrofitClientInstance;
import co.supesi.utility.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OnlineOrders#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OnlineOrders extends Fragment {

    private static final String TAG = "OnlineOrders";
    Unbinder unbinder;
    OnlineOrdersAdapter onlineOrdersAdapter;
    SharedPreferences sharedPref;
    String token;
    @BindView(R.id.rv_online_orders)
    RecyclerView rvOrders;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public OnlineOrders() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OnlineOrders.
     */
    // TODO: Rename and change types and number of parameters
    public static OnlineOrders newInstance(String param1, String param2) {
        OnlineOrders fragment = new OnlineOrders();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_online_orders, container, false);
        unbinder = ButterKnife.bind(this,view);
        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadOnlineOrders(view);
    }

    private void loadOnlineOrders(View view) {
        sharedPref = view.getContext().getSharedPreferences(Constants.getPrefJwt(), Context.MODE_PRIVATE);
        token = sharedPref.getString("jwt",null);
        if (token !=null){
            fetchOnline(token);
        }

    }

    private void fetchOnline(String token) {

        GetData networkData = RetrofitClientInstance.getRetrofitInstance().create(GetData.class);
        Call<ApiReadyOrderResponse> responseCall = networkData.getReadyOrders(token);
        responseCall.enqueue(new Callback<ApiReadyOrderResponse>() {
            @Override
            public void onResponse(Call<ApiReadyOrderResponse> call, Response<ApiReadyOrderResponse> response) {
                if (response.body() != null){
                    String message = response.body().getMessage();
                    if (Boolean.parseBoolean(message)){
                        parseResponse(response.body());
                    }

                }
            }

            @Override
            public void onFailure(Call<ApiReadyOrderResponse> call, Throwable t) {

            }
        });

    }

    private void parseResponse(ApiReadyOrderResponse body) {

        ArrayList<OrderEntry> orderEntries = body.getOrderEntries();

        onlineOrdersAdapter = new OnlineOrdersAdapter(getContext(), orderEntries);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvOrders.setLayoutManager(layoutManager);
        rvOrders.setAdapter(onlineOrdersAdapter);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        onlineOrdersAdapter = null;
    }
}
