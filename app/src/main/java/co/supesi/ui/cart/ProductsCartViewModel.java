package co.supesi.ui.cart;

import android.app.Application;
import android.text.TextUtils;
import android.util.Log;


import androidx.arch.core.util.Function;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.Transformations;

import java.util.List;

import co.supesi.BasicApp;
import co.supesi.database.DataRepository;
import co.supesi.model.Customer;
import co.supesi.model.Order;
import co.supesi.model.Product;
import co.supesi.model.ProductAndStock;

public class ProductsCartViewModel extends AndroidViewModel {

    private static final String TAG = ProductsCartViewModel.class.getSimpleName();

    private DataRepository dataRepository;
    private LiveData<List<Order>> allOrders;

    private LiveData<List<Product>> productFromDb;

    private LiveData<List<ProductAndStock>> productStocks;

    private LiveData<Double> itemsInOrder;
    private LiveData<Integer> itemCount;

    Order hasCustomer;

    private static final String QUERY_KEY = "QUERY";

    private final SavedStateHandle mSavedStateHandler;

    public ProductsCartViewModel(Application application, SavedStateHandle savedStateHandle) {
        super(application);
        this.mSavedStateHandler = savedStateHandle;

        dataRepository = BasicApp.getContext().getRepository();
        allOrders = dataRepository.getAllOrders();

        itemsInOrder = dataRepository.getTotals();
        productStocks = dataRepository.withStocks();

        itemCount = dataRepository.getCount();

        //productFromDb = dataRepository.getAllProducts();
        productFromDb = Transformations.switchMap(
                savedStateHandle.getLiveData("QUERY", null),
                (Function<CharSequence, LiveData<List<Product>>>) query ->{
                    if (TextUtils.isEmpty(query)){
                        return  dataRepository.getAllProducts();
                    }
                    return dataRepository.searchProducts("*" + query + "*");
                }
        );


    }

    public void setQuery(CharSequence query) {
        mSavedStateHandler.set(QUERY_KEY, query);
    }


    public void insertOrderFromViewModel(Order order){
        dataRepository.insertOrder(order);
    }
    public void insertProductFromViewModel(Product product){
        dataRepository.insertProduct(product);

    }

    public LiveData<List<Order>> getOrdersFromDb(){
        return allOrders;
    }
    public LiveData<List<Product>> getProductsFromDb(){return productFromDb;}



//here
    public LiveData<List<ProductAndStock>> getProductsWithStocks(){return productStocks;}

    public void updatedOrder(Order order){
        dataRepository.updateQuantity(order);
        //updatedOrder.setValue(order);

    }
    public void updateCustomerOrder(Customer customer){
        dataRepository.updateCustomerOrder(customer);
    }

    public void deleteOrders(){
        dataRepository.deleteAllOrders();
    }
    public void deleteOneOrder(Order product){
        dataRepository.deleteSingleOrder(product);
    }

    public LiveData<Double> getTotals(){
        return itemsInOrder;
    }


    public void insertOrUpdate(Order order){
        dataRepository.getOrderByProductId(order);

    }


}