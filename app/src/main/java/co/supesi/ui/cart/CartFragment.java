package co.supesi.ui.cart;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.supesi.R;
import co.supesi.adapter.CartAdapter;
import co.supesi.model.ApiOrderResponse;
import co.supesi.model.Order;
import co.supesi.model.Product;
import co.supesi.network.GetData;
import co.supesi.network.RetrofitClientInstance;
import co.supesi.utility.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartFragment extends Fragment {
    private static final String TAG= CartFragment.class.getSimpleName();

    private ProductsCartViewModel productsCartViewModel;
    ArrayList<Product> productArrayList;
    CartAdapter cartAdapter;
    String token;
    SharedPreferences sharedPref;

    @BindView(R.id.rv_cart)
    RecyclerView rvCart;

    @BindView(R.id.tv_total)
    TextView tvTotal;
    NavController navController;

    private Unbinder unbinder;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        unbinder = ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        navController = Navigation.findNavController(view);
        sharedPref = view.getContext().getSharedPreferences(Constants.getPrefJwt(), Context.MODE_PRIVATE);
        token = sharedPref.getString("jwt",null);



        productsCartViewModel =
                new ViewModelProvider(requireActivity()).get(ProductsCartViewModel.class);

        cartAdapter = new CartAdapter(getContext(), productsCartViewModel);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvCart.setLayoutManager(layoutManager);
        rvCart.setAdapter(cartAdapter);

        productsCartViewModel.getOrdersFromDb().observe(getViewLifecycleOwner(), orders -> {
            if (cartAdapter != null){
                cartAdapter.setCartItems(orders);
            }

        });


        productsCartViewModel.getTotals().observe(getViewLifecycleOwner(), new Observer<Double>() {
            @Override
            public void onChanged(Double aFloat) {

                if (aFloat != null){
                    showText(aFloat);

                }

            }

        });


    }
    public void showText(Double f){
        if (f != null){
            tvTotal.setText(String.valueOf(f));
        }else{
            tvTotal.setText("nothing");
        }


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();


        // TODO: 2020-03-04 Find out why unbinder not working with live data
        unbinder.unbind();
        cartAdapter = null;
    }


    @OnClick(R.id.btn_pay_now)
    public void payNow(){
        //go to payment fragment
        Bundle bundle = new Bundle();
        bundle.putString("amount", (tvTotal.getText().toString()));
        Navigation.findNavController(tvTotal).navigate(R.id.action_pay_now, bundle);
    }

    @OnClick(R.id.btn_clear)
    public void clearCart(){
        Toast.makeText(getContext(), "Cart cleared", Toast.LENGTH_SHORT).show();
        productsCartViewModel.deleteOrders();

        navController.popBackStack(R.id.nav_home,false);
    }
}