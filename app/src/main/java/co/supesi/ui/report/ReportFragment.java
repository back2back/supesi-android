package co.supesi.ui.report;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.R;
import co.supesi.model.ApiReportResponse;
import co.supesi.model.Order;
import co.supesi.model.OrderEntry;
import co.supesi.network.GetData;
import co.supesi.network.RetrofitClientInstance;
import co.supesi.utility.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReportFragment extends Fragment {

    private static final String TAG = ReportFragment.class.getSimpleName();
    private Unbinder unbinder;
    SharedPreferences sharedPref;
    String token;
    @BindView(R.id.tv_total)
    TextView tvTodayTotal;
    @BindView(R.id.chart)
    LineChart lineChart;
    @BindView(R.id.tv_avg)
    TextView tvAvg;

    public ReportFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_report, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        fetchReportsOnline(view);
    }


    private void fetchReportsOnline(View view) {
        GetData getReportData = RetrofitClientInstance.getRetrofitInstance().create(GetData.class);
        sharedPref = view.getContext().getSharedPreferences(Constants.getPrefJwt(), Context.MODE_PRIVATE);
        token = sharedPref.getString("jwt",null);

        final View root = view;

        Call<ApiReportResponse> apiReportResponseCall = getReportData.getTodayReport(token);
        apiReportResponseCall.enqueue(new Callback<ApiReportResponse>() {
            @Override
            public void onResponse(Call<ApiReportResponse> call, Response<ApiReportResponse> response) {
                if (response.body() != null){
                    processReport(response.body());
                }
            }

            @Override
            public void onFailure(Call<ApiReportResponse> call, Throwable t) {
                Snackbar.make(root,
                        "Error getting reports",
                        Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void processReport(ApiReportResponse body) {
        String message = body.getMessage();
        boolean foo = Boolean.parseBoolean(message);
        if (foo){
            String zz = body.getTotal();
            tvTodayTotal.setText(zz);

            int total = Integer.parseInt(zz);
            int count = body.getOrderEntries().size();

            try {

            }catch (ArithmeticException e){

            }
            if (total > 0){
                int avg = total/count;
                tvAvg.setText(getString(R.string.kes) + avg);
            }else{
                tvAvg.setText(getString(R.string.zero_money, 0f));
            }


            List<Entry> entries = new ArrayList<>();

            for (OrderEntry p: body.getOrderEntries()){
                ArrayList<Order> inside = p.getOrders();
                float x=0;
                for (Order o: inside){
                    x =  o.getCalculatedTotal();

                }

                entries.add( new Entry(p.getId(), x) );

            }



            LineDataSet dataSet = new LineDataSet(entries, "Prices");
            dataSet.setColor(R.color.colorPrimary);

            LineData lineData = new LineData(dataSet);
            lineChart.setNoDataText(getString(R.string.tap_to_see_char));
            lineChart.setDrawGridBackground(false);

            lineChart.setData(lineData);
            lineChart.invalidate();

        }

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
