package co.supesi.ui.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.navigation.NavigationView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.supesi.BasicApp;
import co.supesi.MainActivity;
import co.supesi.R;
import co.supesi.adapter.ProductsAdapter;
import co.supesi.model.ApiCustomerResponse;
import co.supesi.model.ApiStoreProductResponse;
import co.supesi.model.ApiUserResponse;
import co.supesi.model.Customer;
import co.supesi.model.OnProductClickListener;
import co.supesi.model.Order;
import co.supesi.model.Product;
import co.supesi.model.StoreProduct;
import co.supesi.model.User;
import co.supesi.network.GetData;
import co.supesi.network.RetrofitClientInstance;
import co.supesi.ui.LoginViewModel;
import co.supesi.ui.RegistrationViewModel;
import co.supesi.ui.UserDataViewModel;
import co.supesi.ui.cart.ProductsCartViewModel;
import co.supesi.utility.Constants;
import co.supesi.utility.FontManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class HomeFragment extends Fragment implements OnProductClickListener {

    ProductsCartViewModel productsCartViewModel;
    ProductsAdapter productsAdapter;

    Integer count ;
    Double total ;
    private final static String TAG = "HomeFragment";


    private LoginViewModel loginViewModel;
    RegistrationViewModel registrationViewModel;
    SharedPreferences sharedPref;
    String token;

    @BindView(R.id.pull_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.rv_products)
    RecyclerView recyclerView;


    @BindView(R.id.btn_home_checkout)
    Button btnCart;
    @BindView(R.id.btn_search)
    ImageButton iBSearch;
    @BindView(R.id.et_search_product)
    AppCompatEditText etSearch;
    private Unbinder unbinder;

    UserDataViewModel userDataViewModel;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this,view);
        return view;
    }



    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        productsCartViewModel = new ViewModelProvider(requireActivity()).get(ProductsCartViewModel.class);
        userDataViewModel = new ViewModelProvider(requireActivity()).get(UserDataViewModel.class);
        loginViewModel = new ViewModelProvider(requireActivity()).get(LoginViewModel.class);
        registrationViewModel = new ViewModelProvider(requireActivity()).get(RegistrationViewModel.class);

        final NavController navController = Navigation.findNavController(view);
        loginViewModel.getAuthState().observe(getViewLifecycleOwner(), new Observer<LoginViewModel.AuthenticationState>() {
            @Override
            public void onChanged(LoginViewModel.AuthenticationState authenticationState) {
                switch (authenticationState){
                    case AUTHENTICATED:
                        loadProducts(view);
                        break;
                    case UNAUTHENTICATED:
                        navController.navigate(R.id.nav_login);
                        break;
                }
            }
        });

        registrationViewModel.getRegistrationStateMutableLiveData().observe(
                getViewLifecycleOwner(), registrationState -> {
                    if (registrationState == RegistrationViewModel.RegistrationState.REGISTRATION_COMPLETED){
                        //Here we authenticate with the token provided by the ViewModel
                        // then pop back to the profie_fragment, where the user authentication
                        // status will be tested and should be authenticated.
                        //todo user should register and be logged in

                        loadProducts(view);
                        //navController.navigate(R.id.nav_home);
                        navController.popBackStack(R.id.nav_home,false);

                    }
                }
        );
        checkUser();

        swipeRefreshLayout.setOnRefreshListener(() ->
                fetchOnline()
                //checkUser()
        );
        getCartItems();

        iBSearch.setOnClickListener(v ->{
            Editable query = etSearch.getText();
            productsCartViewModel.setQuery(query);
            hideKeyboard(v);
        });



        etSearch.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (etSearch.getRight() - etSearch.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here


                        foobar(v);



                        return true;
                    }
                }
                return false;
            }
        });
    }

    private void foobar(View view) {
        etSearch.setText("");
        productsCartViewModel.setQuery("");
        InputMethodManager imm =(InputMethodManager) this.requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        view.clearFocus();
    }

    private void hideKeyboard(View view){
        InputMethodManager imm =(InputMethodManager) this.requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        view.clearFocus();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        productsAdapter = null;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    void getCartItems(){

        productsCartViewModel.getTotals().observe(getViewLifecycleOwner(), baz -> total = baz);
        if (productsCartViewModel.getTotals().getValue() != null){
            total = productsCartViewModel.getTotals().getValue();
            buttonAmount(total);
        }

    }
    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    void buttonAmount(double total){

        btnCart.setText("KES"+" "+ String.format("%.2f", total));

    }
    private void loadProducts(View view) {
        sharedPref = view.getContext().getSharedPreferences(Constants.getPrefJwt(), MODE_PRIVATE);
        token = sharedPref.getString("jwt",null);
        if (token !=null){
            //fetchOnline();
        }


        productsAdapter = new ProductsAdapter(getContext(), productsCartViewModel, this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(productsAdapter);

        productsCartViewModel.getProductsFromDb().observe(getViewLifecycleOwner(), products -> productsAdapter.setProducts(products));
        btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.action_nav_home_to_nav_gallery);
            }
        });



    }


    //todo make app fetch only what we don't have
    private void fetchOnline(){
        GetData getData = RetrofitClientInstance.getRetrofitInstance().create(GetData.class);
        Call<ApiStoreProductResponse> listCall = getData.getAllProducts(token);
        listCall.enqueue(new Callback<ApiStoreProductResponse>() {

            @Override
            public void onResponse(Call<ApiStoreProductResponse> call, Response<ApiStoreProductResponse> response) {

                if (response.body() != null){

                    hallo(response.body());
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<ApiStoreProductResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Error getting products from server", Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }

        });
    }

    private void hallo(ApiStoreProductResponse response){
        List<StoreProduct> storeProducts = response.getStoreProducts();
        for (int i = 0; i < storeProducts.size(); i++){
            Product product = storeProducts.get(i).getProduct();
            product.setStoreId(storeProducts.get(i).getStoreId());
            productsCartViewModel.insertProductFromViewModel(product);
        }

    }
    private Boolean checkJwt(){
        sharedPref = requireActivity().getSharedPreferences(Constants.getPrefJwt(),MODE_PRIVATE);
        token = sharedPref.getString("jwt", null);
        if (token == null){
            return  false;
        }else{
            return true;
        }
    }
    private void checkUser(){
        userDataViewModel.getUser().observe(requireActivity(), new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> users) {
                if (users.size() == 0){
                    if (checkJwt()){
                        profile();
                    }
                }
            }
        });
    }
    private void profile() {

        GetData getData = RetrofitClientInstance.getRetrofitInstance().create(GetData.class);
        Call<ApiUserResponse> getProf = getData.getProfile(token);
        getProf.enqueue(new Callback<ApiUserResponse>() {
            @Override
            public void onResponse(Call<ApiUserResponse> call, Response<ApiUserResponse> response) {
                if (response.body() != null) {
                    ApiUserResponse apiUserResponse = response.body();
                    boolean msg = Boolean.parseBoolean(apiUserResponse.getMessage());
                    if (msg) {

                        new UserDataViewModel(requireActivity().getApplication()).saveUserData(apiUserResponse);


                    }
                }
            }

            @Override
            public void onFailure(Call<ApiUserResponse> call, Throwable t) {

            }
        });

    }


    @Override
    public void onProductClick(Product current) {


        //check if product id is in db

//        productsCartViewModel.getOrderByProdId(current).observe(getViewLifecycleOwner(), order -> Log.e(TAG, order.getProductName()));

        productsCartViewModel.getTotals().observe(getViewLifecycleOwner(), baz -> {
            //total = baz;
            if (baz !=null){
                buttonAmount(baz);
            }

        });


        Order order = new Order();
        order.setProduct_id(current.getId());
        order.setProductName(current.getTitle());
        order.setQuantity(1);
        order.setPrice(current.getSellingPrice());
        order.setStoreId(current.getStoreId());
        productsCartViewModel.insertOrUpdate(order);
    }
}