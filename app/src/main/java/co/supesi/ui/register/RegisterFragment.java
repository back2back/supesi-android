package co.supesi.ui.register;


import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.supesi.R;
import co.supesi.ui.LoginViewModel;
import co.supesi.ui.RegistrationViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {

    private LoginViewModel loginViewModel;
    private RegistrationViewModel registrationViewModel;
    @BindView(R.id.et_tenant)
    TextInputEditText etTenant;
    @BindView(R.id.et_email)
    TextInputEditText eTEmail;
    @BindView(R.id.et_password)
    TextInputEditText eTPassword;
    @BindView(R.id.btn_register)
    Button btnRegister;
    @BindView(R.id.tv_login_instead)
    TextView tvLoginInstead;
    @BindView(R.id.et_first_name)
    TextInputEditText etFirstName;
    @BindView(R.id.et_last_name)
    TextInputEditText etLastName;

    @BindView(R.id.et_phone)
    TextInputEditText etPhone;

    private Unbinder unbinder;
    NavController navController;

    private static String TAG = "RegisterFragment";

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_register, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registrationViewModel = new ViewModelProvider(requireActivity()).get(RegistrationViewModel.class);
        loginViewModel = new ViewModelProvider(requireActivity()).get(LoginViewModel.class);

        //final NavController navController = Navigation.findNavController(view);
         navController = Navigation.findNavController(view);
        //checkTenantName();
        //watchBusinessName();
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrationViewModel.createAccount(
                        eTEmail.getText().toString(),
                        eTPassword.getText().toString(),
                        etTenant.getText().toString(),
                        etPhone.getText().toString(),
                        etFirstName.getText().toString(),
                        etLastName.getText().toString()

                );
            }
        });



        registrationViewModel.getRegistrationStateMutableLiveData().observe(
                getViewLifecycleOwner(), new Observer<RegistrationViewModel.RegistrationState>() {
                    @Override
                    public void onChanged(RegistrationViewModel.RegistrationState registrationState) {
                        if (registrationState == RegistrationViewModel.RegistrationState.REGISTRATION_COMPLETED){
                            //Here we authenticate with the token provided by the ViewModel
                            // then pop back to the profie_fragment, where the user authentication
                            // status will be tested and should be authenticated.
                            //todo user should register and be logged in

                            //navController.navigate(R.id.nav_home);
                            navController.popBackStack(R.id.nav_home,false);

                        }
                    }
                }
        );
        // If the user presses back, cancel the user registration and pop back
        // to the login fragment. Since this ViewModel is shared at the activity
        // scope, its state must be reset so that it will be in the initial
        // state if the user comes back to register later.
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(),
                new OnBackPressedCallback(true) {
                    @Override
                    public void handleOnBackPressed() {
                        registrationViewModel.userCancelledRegistration();
                        navController.popBackStack(R.id.nav_home, false);
                    }
                });


    }

    private void watchBusinessName() {
        etTenant.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Toast.makeText(requireActivity(), "before", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (start >  2){
                    //String inputString = String.valueOf(s.subSequence(start, start + count));
                    Toast.makeText(requireActivity(), s, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Log.e(TAG, "foo "+s);
                //Toast.makeText(requireActivity(), s, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*
    @return
     */
    private boolean checkTenantName() {
        return false;
    }

    @OnClick(R.id.tv_login_instead)
    public void supportChat(){
        //Snackbar.make(btnRegister, "Call 0716666309", BaseTransientBottomBar.LENGTH_LONG).show();
        navController.popBackStack(R.id.nav_home, false);
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
