package co.supesi.ui;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;


import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import co.supesi.model.ApiUserResponse;
import co.supesi.model.User;
import co.supesi.network.GetData;
import co.supesi.network.RetrofitClientInstance;
import co.supesi.utility.Constants;
import co.supesi.utility.SaveUser;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationViewModel extends AndroidViewModel {
    private String jwt;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    public static final String TAG = RegistrationViewModel.class.getSimpleName();

    public RegistrationViewModel(Application application){
        super(application);
    }

    public enum RegistrationState{
        REGISTRATION_COMPLETED,
        COLLECT_PROFILE_DATA
    }

    private MutableLiveData<RegistrationState> registrationStateMutableLiveData
            = new MutableLiveData<>();

    public MutableLiveData<RegistrationState> getRegistrationStateMutableLiveData(){
        return registrationStateMutableLiveData;
    }

    public String getJwt() {
        return jwt;
    }

    public void createAccount(String email, String password, String tenantName, String phone, String fName, String lName){
        //registrationStateMutableLiveData.setValue(RegistrationState.REGISTRATION_COMPLETED);
        GetData getData = RetrofitClientInstance.getRetrofitInstance().create(GetData.class);
        Call<ApiUserResponse> addUser = getData.addUser(new User(email,password,tenantName,phone, fName, lName));
        addUser.enqueue(new Callback<ApiUserResponse>() {
            @Override
            public void onResponse(Call<ApiUserResponse> call, Response<ApiUserResponse> response) {
                if (response.body() != null){
                    ApiUserResponse apiUserResponse = response.body();
                    boolean msg = Boolean.parseBoolean(apiUserResponse.getMessage());
                    if (msg){
                        writePref(apiUserResponse.getJwt());
                        RegistrationViewModel.this.registrationStateMutableLiveData.setValue(RegistrationState.REGISTRATION_COMPLETED);
                        new SaveUser(getApplication()).saveUserData(apiUserResponse);
                    }else {
                        RegistrationViewModel.this.registrationStateMutableLiveData.setValue(RegistrationState.COLLECT_PROFILE_DATA);
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiUserResponse> call, Throwable t) {

            }
        });

    }
    public boolean userCancelledRegistration() {
        // Clear existing registration data
        registrationStateMutableLiveData.setValue(RegistrationState.COLLECT_PROFILE_DATA);

        return true;
    }

    private void writePref(String jwt){
        preferences = getApplication().getSharedPreferences(Constants.getPrefJwt(), Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString("jwt", jwt);
        editor.commit();
    }

}
