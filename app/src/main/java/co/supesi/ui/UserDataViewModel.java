package co.supesi.ui;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import co.supesi.BasicApp;
import co.supesi.database.DataRepository;
import co.supesi.model.ApiUserResponse;
import co.supesi.model.Tenant;
import co.supesi.model.User;


public class UserDataViewModel extends AndroidViewModel {

    private static final String TAG = UserDataViewModel.class.getSimpleName();
    private DataRepository dataRepository;
    private LiveData<List<User>> user;
    private LiveData<List<Tenant>> tenant;

    public UserDataViewModel(@NonNull Application application) {
        super(application);

        dataRepository = BasicApp.getContext().getRepository();
        user = dataRepository.getUser();
        tenant = dataRepository.getTenant();
    }

    public LiveData<List<User>> getUser(){
        return user;
    }

    public LiveData<List<Tenant>> getTenant(){
        return tenant;
    }
    public void saveUserData(ApiUserResponse apiUserResponse) {
        User user = apiUserResponse.getSingleUser();
        Tenant tenant = apiUserResponse.getTenant();
        dataRepository.insertUser(user,tenant);
    }
}
