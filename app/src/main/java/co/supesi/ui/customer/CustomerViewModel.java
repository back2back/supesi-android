package co.supesi.ui.customer;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import co.supesi.BasicApp;
import co.supesi.database.DataRepository;
import co.supesi.model.Customer;

public class CustomerViewModel extends AndroidViewModel {

    private DataRepository dataRepository;
    private LiveData<List<Customer>> customers;
    public CustomerViewModel(@NonNull @NotNull Application application) {
        super(application);
        dataRepository = BasicApp.getContext().getRepository();
        customers = dataRepository.getCustomers();
    }

    public LiveData<List<Customer>> getCustomers(){
        return customers;
    }

    public void insert(Customer customer){
        dataRepository.insertCustomer(customer);
    }

    public LiveData<Customer> getCustomerById(int customerId){
        return dataRepository.getCustomerById(customerId);
    }
}
