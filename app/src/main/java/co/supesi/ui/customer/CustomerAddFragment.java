package co.supesi.ui.customer;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.R;
import co.supesi.model.ApiCustomerResponse;
import co.supesi.model.Customer;
import co.supesi.network.GetData;
import co.supesi.network.RetrofitClientInstance;
import co.supesi.ui.cart.ProductsCartViewModel;
import co.supesi.utility.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CustomerAddFragment extends Fragment {

    private static final String TAG = "CustomerAddFragment";

    SharedPreferences sharedPreferences;
    String token;
    CustomerViewModel customerViewModel;
    ProductsCartViewModel productsCartViewModel;

    @BindView(R.id.btn_customer_add)
    Button btnAddCustomer;
    @BindView(R.id.et_customer_name)
    EditText etCustomerName;
    @BindView(R.id.et_phone)
    EditText etCustomerPhone;
    private Unbinder unbinder;
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_customer_add, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable  Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sharedPreferences = requireActivity().getSharedPreferences(Constants.getPrefJwt(), Context.MODE_PRIVATE);

        customerViewModel = new ViewModelProvider(requireActivity()).get(CustomerViewModel.class);
        productsCartViewModel = new ViewModelProvider(requireActivity()).get(ProductsCartViewModel.class);

        token = sharedPreferences.getString("jwt",null);

        btnAddCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String customerName = etCustomerName.getText().toString();
                String phone = etCustomerPhone.getText().toString();
                if (customerName.isEmpty()){
                    Toast.makeText(requireActivity(), "Not allowed", Toast.LENGTH_SHORT).show();
                }else{
                    createCustomer(customerName, phone);

                }
            }
        });

    }

    private void createCustomer(String username, String phone) {
        //required params username,phone
        // create and attach to the order

        int phone2 = Integer.parseInt(phone);
        GetData createCustomer = RetrofitClientInstance.getRetrofitInstance().create(GetData.class);
        Customer customer = new Customer(username, phone2);

        Call<ApiCustomerResponse> call = createCustomer.addCustomer(token, customer);

        call.enqueue(new Callback<ApiCustomerResponse>() {
            @Override
            public void onResponse(Call<ApiCustomerResponse> call, Response<ApiCustomerResponse> response) {
                if (response.body() != null){

                    ApiCustomerResponse response1 = response.body();
                    boolean msg = (Boolean.parseBoolean(response1.getMessage()));
                    if (msg){
                        //add customer to the order
                        customerToOrder(response1.getCustomer());
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiCustomerResponse> call, Throwable t) {
                //even on fail go back
                Toast.makeText(requireActivity(), "Check internet", Toast.LENGTH_SHORT).show();
                Navigation.findNavController(view).navigate(R.id.back_to_pay);
            }
        });

    }

    private void customerToOrder(Customer customer) {
        //insert customer to db
        createCustomer(customer);
        //update order with customer
        // id
        updateOrderWithCustomer(customer);
        //navigate back
        Navigation.findNavController(view).navigate(R.id.back_to_pay);

    }

    private void updateOrderWithCustomer(Customer customer) {
        productsCartViewModel.updateCustomerOrder(customer);
    }

    private void createCustomer(Customer customer){
        customerViewModel.insert(customer);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}