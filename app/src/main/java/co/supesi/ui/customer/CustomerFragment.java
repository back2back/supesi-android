package co.supesi.ui.customer;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.R;
import co.supesi.adapter.CustomerAdapter;
import co.supesi.database.OnCustomerClickListener;
import co.supesi.model.ApiCustomerResponse;
import co.supesi.model.Customer;
import co.supesi.model.Product;
import co.supesi.network.GetData;
import co.supesi.network.RetrofitClientInstance;
import co.supesi.ui.cart.ProductsCartViewModel;
import co.supesi.ui.home.HomeFragment;
import co.supesi.utility.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;


public class CustomerFragment extends Fragment implements OnCustomerClickListener {

    private Unbinder unbinder;
    private final static String TAG = "CustomerFragment";
    SharedPreferences sharedPref;
    String token;

    CustomerViewModel customerViewModel;
    ProductsCartViewModel productsCartViewModel;

    ProgressDialog progressDialog;
    CustomerAdapter customerAdapter;
    @BindView(R.id.rv_customers)
    RecyclerView rvCustomers;
    @BindView(R.id.iv_customer_add)
    ImageView ivCustomerAdd;

    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_customer, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        customerViewModel = new ViewModelProvider(requireActivity()).get(CustomerViewModel.class);
        productsCartViewModel = new ViewModelProvider(requireActivity()).get(ProductsCartViewModel.class);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Getting customer");
        progressDialog.show();


        customerAdd();
        //get customers
        getCustomer(view);
    }

    private void customerAdd() {
        ivCustomerAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_customer_add);
            }
        });
    }


    private void getCustomer(View view){
        Log.e(TAG, "Getting customer");
        sharedPref = view.getContext().getSharedPreferences(Constants.getPrefJwt(), MODE_PRIVATE);
        token = sharedPref.getString("jwt",null);
        GetData getData = RetrofitClientInstance.getRetrofitInstance().create(GetData.class);
        Call<ApiCustomerResponse> listCustomers = getData.getCustomers(token);
        listCustomers.enqueue(new Callback<ApiCustomerResponse>() {
            @Override
            public void onResponse(Call<ApiCustomerResponse> call, Response<ApiCustomerResponse> response) {
                if (response.body() != null){
                    progressDialog.dismiss();
                    List<Customer> customers = response.body().getCustomers();
                    parseCustomers(customers);

                }
            }

            @Override
            public void onFailure(Call<ApiCustomerResponse> call, Throwable t) {
                progressDialog.dismiss();
                Log.e(TAG, "failed");
            }
        });
    }

    private void parseCustomers(List<Customer> customers) {
        customerAdapter = new CustomerAdapter(getContext(), (ArrayList<Customer>) customers, this);
        customerAdapter.setCustomerItems(customers);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvCustomers.setLayoutManager(layoutManager);
        rvCustomers.setAdapter(customerAdapter);

        for (Customer c : customers){
            customerViewModel.insert(c);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        customerAdapter = null;
    }

    @Override
    public void onCustomerClick(Customer customer) {
        productsCartViewModel.updateCustomerOrder(customer);
        //Navigation.findNavController(view).navigate(R.id.back_to_pay);
    }
}