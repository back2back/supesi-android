package co.supesi.ui.productmanage;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.R;
import co.supesi.adapter.ManageProductsAdapter;
import co.supesi.adapter.ProductsAdapter;
import co.supesi.model.Product;
import co.supesi.model.ProductAndStock;
import co.supesi.model.Stock;
import co.supesi.ui.cart.ProductsCartViewModel;
import co.supesi.ui.home.HomeFragment;

public class ProductManage extends Fragment {

    private Unbinder unbinder;
    ManageProductsAdapter productsAdapter;


    StocksViewModel stocksViewModel;


    @BindView(R.id.rv_prod_overview)
    RecyclerView recyclerView;


    @BindView(R.id.btn_create_prod)
    Button btnCreateProd;
    private final static String TAG = "ProductManage";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_prod_overview, container, false);

        unbinder = ButterKnife.bind(this,view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        stocksViewModel = new ViewModelProvider(requireActivity()).get(StocksViewModel.class);

        stocksViewModel.getAllProductStocks().observe(getViewLifecycleOwner(), new Observer<List<ProductAndStock>>() {
            @Override
            public void onChanged(List<ProductAndStock> productAndStocks) {
                productsAdapter.setProducts1(productAndStocks);
            }
        });

        productsAdapter = new ManageProductsAdapter(getContext());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(productsAdapter);




        btnCreateProd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.action_nav_manage_to_nav_create_product);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        productsAdapter = null;

    }
}
