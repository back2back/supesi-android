package co.supesi.ui.productmanage;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import co.supesi.BasicApp;
import co.supesi.database.DataRepository;
import co.supesi.model.ProductAndStock;
import co.supesi.model.Stock;

public class StocksViewModel extends AndroidViewModel {

    private DataRepository dataRepository;
    private LiveData<List<Stock>> allStocks;
    private LiveData<Stock> justOne;
    private LiveData<List<ProductAndStock>> productAndStock;

    public StocksViewModel(@NonNull Application application) {
        super(application);
        dataRepository = BasicApp.getContext().getRepository();
        allStocks = dataRepository.getStocks();
        productAndStock = dataRepository.withStocks();
    }

     public LiveData<List<Stock>> getAllStocks(){
        return allStocks;
    }
    public LiveData<List<ProductAndStock>> getAllProductStocks(){
        return productAndStock;
    }

    public LiveData<Stock> singleStock(int id){
        return  dataRepository.mine(id);
    }
    public void insert(Stock stock){
        dataRepository.insertStock(stock);
    }

}
