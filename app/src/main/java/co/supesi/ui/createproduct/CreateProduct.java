package co.supesi.ui.createproduct;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.R;
import co.supesi.model.ApiStoreProductResponse;
import co.supesi.model.Product;
import co.supesi.network.GetData;
import co.supesi.network.RetrofitClientInstance;
import co.supesi.utility.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CreateProduct extends Fragment {

    @BindView(R.id.et_price)
    EditText etSellingPrice;
    @BindView(R.id.et_prod_name_create)
    EditText etProductName;
    @BindView(R.id.et_prod_description)
    EditText etDescription;
    @BindView(R.id.btn_prod_add)
    Button btnCreateProduct;
    @BindView(R.id.et_prod_stock)
    EditText etStock;

    ProgressDialog progressDialog;

    String productName;
    String sellingPrice;
    String description;
    String stockCount;

    SharedPreferences sharedPref;
    String token;

    private final static String TAG = CreateProduct.class.getSimpleName();

    private Unbinder unbinder;

    public CreateProduct() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_product, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        token = getToken(view);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Creating product");
        btnCreateProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog.show();
                //get the data
                productName = etProductName.getText().toString();
                sellingPrice = etSellingPrice.getText().toString();
                description = etDescription.getText().toString();
                stockCount = etStock.getText().toString();

                if (stockCount.equals("")){
                    stockCount = "0";
                }
                if (sellingPrice == null){
                    Toast.makeText(requireActivity(), "Must set price", Toast.LENGTH_SHORT).show();
                }else{

                    float sp = Float.parseFloat(sellingPrice);
                    int stk = Integer.parseInt(stockCount);

                    createProduct(productName, sp, description);
                    etProductName.getText().clear();
                    etSellingPrice.getText().clear();
                    etDescription.getText().clear();
                    etStock.getText().clear();
                }

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void createProduct(String productName, Float sellingPrice, String description){

        Product product = new Product(productName, sellingPrice, 0f,description);
        GetData createProduct = RetrofitClientInstance.getRetrofitInstance().create(GetData.class);
        Call<ApiStoreProductResponse> listCall = createProduct.addProduct(product,token);

        listCall.enqueue(new Callback<ApiStoreProductResponse>() {
            @Override
            public void onResponse(Call<ApiStoreProductResponse> call, Response<ApiStoreProductResponse> response) {


                //notify user of success
                if (response.body() != null){
                    progressDialog.dismiss();

                    ApiStoreProductResponse apiStoreProductResponse = response.body();
                    boolean msg = (Boolean.parseBoolean(apiStoreProductResponse.getMessage()));
                    if (msg){
                        Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getActivity(), "Error creating product", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiStoreProductResponse> call, Throwable t) {

                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Error creating data", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private String getToken(View view){
        sharedPref = view.getContext().getSharedPreferences(Constants.getPrefJwt(), Context.MODE_PRIVATE);
        token = sharedPref.getString("jwt",null);
        if (token !=null){
            //fetchOnline();
            return  token;
        }
        return "no-token";
    }
}
