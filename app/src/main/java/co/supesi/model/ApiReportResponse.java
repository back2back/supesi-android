package co.supesi.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ApiReportResponse {
    @SerializedName("message")
    private String message;

    @SerializedName("total")
    private String total;

    @SerializedName("orderEntries")
    private ArrayList<OrderEntry> orderEntries;

    public ArrayList<OrderEntry> getOrderEntries() {
        return orderEntries;
    }

    public void setOrderEntries(ArrayList<OrderEntry> orderEntries) {
        this.orderEntries = orderEntries;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
