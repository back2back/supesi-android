package co.supesi.model;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "teams")
public class Team {
    @PrimaryKey
    @SerializedName("id")
    private int id;
    @SerializedName("tenant_id")
    private int tenantId;
    @SerializedName("org_role")
    private int orgRole;
    @SerializedName("user_id")
    private int userId;
    @SerializedName("user")
    @Ignore
    private User user;

    private String firstName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

    public int getOrgRole() {
        return orgRole;
    }

    public void setOrgRole(int orgRole) {
        this.orgRole = orgRole;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return firstName;
    }
}
