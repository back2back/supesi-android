package co.supesi.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiCustomerResponse {

    @SerializedName("message")
    private String message;

    @SerializedName("customers")
    private List<Customer> customers;

    @SerializedName("customer")
    private Customer customer;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
