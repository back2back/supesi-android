package co.supesi.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "orders",
        foreignKeys = {
            @ForeignKey( entity = Product.class,
                    parentColumns = "id",
                    childColumns = "product_id",
                    onDelete = ForeignKey.CASCADE)},
        indices = {@Index(value = "product_id")}
                    )
public class Order {
    private int localId;

    @PrimaryKey()
    @SerializedName("product_id")
    private Integer product_id;

    @ColumnInfo(name = "product_name")
    private String productName;

    @ColumnInfo(name = "qty")
    @SerializedName("qty")
    private Integer quantity;



    @ColumnInfo(name = "sell_at")

    private Float price;

    @SerializedName("total")
    @Ignore
    private int calculatedTotal;

    @SerializedName("product")
    @Ignore
    private Product product;

    @Ignore
    @SerializedName("mode")
    private int orderMode; // 0 from staff, 1 from world;

    @Ignore
    @SerializedName("prepared_by")
    private int preparedBy;

    @Ignore
    @SerializedName("payment_type")
    private int paymentType;

    @SerializedName("store_id")
    private int storeId;


    @SerializedName("customer_id")
    private int customerId;
    @Ignore
    public Order(){

    }

   public Order(int product_id, String productName, int quantity, float price){
        this.product_id = product_id;
        this.productName = productName;
        this.quantity = quantity;
        this.price = price;

    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getOrderMode() {
        return orderMode;
    }

    public void setOrderMode(int orderMode) {
        this.orderMode = 0;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Integer product_id) {
        this.product_id = product_id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public int getLocalId() {
        return localId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getCalculatedTotal() {
        return calculatedTotal;
    }

    public void setCalculatedTotal(int calculatedTotal) {
        this.calculatedTotal = calculatedTotal;
    }

    public int getPreparedBy() {
        return preparedBy;
    }

    public void setPreparedBy(int preparedBy) {
        this.preparedBy = preparedBy;
    }

    public int getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(int paymentType) {
        this.paymentType = paymentType;
    }

    @Override
    public String toString() {
        return "Order{" +
                "localId=" + localId +
                ", product_id=" + product_id +
                ", productName='" + productName + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                ", calculatedTotal=" + calculatedTotal +
                ", product=" + product +
                ", orderMode=" + orderMode +
                ", preparedBy=" + preparedBy +
                ", paymentType=" + paymentType +
                ", storeId=" + storeId +
                ", customerId=" + customerId +
                '}';
    }
}
