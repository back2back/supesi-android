package co.supesi.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ApiReadyOrderResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("orderEntries")
    private ArrayList<OrderEntry> orderEntries;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<OrderEntry> getOrderEntries() {
        return orderEntries;
    }

    public void setOrderEntries(ArrayList<OrderEntry> orderEntries) {
        this.orderEntries = orderEntries;
    }
}
