package co.supesi.model;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.io.Serializable;

public class ProductAndStock implements Serializable {
    @Embedded public  Product product;
    @Relation(
            parentColumn = "id",
            entityColumn = "productId"
    )
    public Stock stock;
}
