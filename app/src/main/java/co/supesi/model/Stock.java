package co.supesi.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.Relation;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "stocks")

public class Stock implements Serializable {

    @PrimaryKey
    @NonNull
    @SerializedName("id")
    private int idStock;

    @SerializedName("nice_counted")
    private int counted;
    @SerializedName("product_id")
    private int productId;

    public Stock(int idStock, int counted, int productId) {
        this.idStock = idStock;
        this.counted = counted;
        this.productId = productId;
    }

    public int getIdStock() {
        return idStock;
    }

    public void setIdStock(int id) {
        this.idStock = id;
    }

    public int getCounted() {
        return counted;
    }

    public void setCounted(int counted) {
        this.counted = counted;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
}
