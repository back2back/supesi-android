package co.supesi.model;

import com.google.gson.annotations.SerializedName;

public class StockRetake {
    @SerializedName("id")
    private int id;
    @SerializedName("qty")
    private int qty;
    @SerializedName("reason")
    private int reason;
    @SerializedName("comment")
    private String comment;
    @SerializedName("stock_id")
    private int stockId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getReason() {
        return reason;
    }

    public void setReason(int reason) {
        this.reason = reason;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getStockId() {
        return stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }
}
