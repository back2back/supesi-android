package co.supesi.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "users")
public class User {

    @SerializedName("id")
    @PrimaryKey
    @NonNull
    private Integer id;

    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;

    @Ignore
    @SerializedName("tenantName")
    private String tenantName;

    @Ignore
    @SerializedName("f_name")
    private String firstName;
    @Ignore
    @SerializedName("l_name")
    private String lastName;

    @Ignore
    @SerializedName("phone")
    private String phone;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    @Ignore
    public User(String email, String password, String tenantName) {
        this.email = email;
        this.password = password;
        this.tenantName = tenantName;
    }

    @Ignore
    public User(String email, String password, String tenantName, String phone, String firstName, String lastName) {
        this.email = email;
        this.password = password;
        this.tenantName = tenantName;
        this.phone = phone;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @NonNull
    public Integer getId() {
        return id;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
