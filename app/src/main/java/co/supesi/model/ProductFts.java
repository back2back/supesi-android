package co.supesi.model;

import androidx.room.Entity;
import androidx.room.Fts4;

@Entity(tableName = "productsFts")
@Fts4(contentEntity = Product.class)
public class ProductFts {

    private String title;
    private String description;

    public ProductFts(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle(){
        return title;
    }

    public String getDescription() {
        return description;
    }
}
