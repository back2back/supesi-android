package co.supesi.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiStockRetake {
    @SerializedName("message")
    private String message;
    @SerializedName("StockRetake")
    private List<StockRetake> stockRetakes;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<StockRetake> getStockRetakes() {
        return stockRetakes;
    }

    public void setStockRetakes(List<StockRetake> stockRetakes) {
        this.stockRetakes = stockRetakes;
    }
}
