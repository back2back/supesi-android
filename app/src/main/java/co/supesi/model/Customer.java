package co.supesi.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "customers")
public class Customer {

    @PrimaryKey
    @NonNull
    @SerializedName("id")
    private Integer id;
    @SerializedName("username")
    private String username;
    @SerializedName("phone")
    private Integer phone;
    @SerializedName("store_id")
    private Integer storeId;
    @SerializedName("tenant_id")
    private Integer tenantId;

    public Customer(@NonNull Integer id, String username, Integer phone, Integer storeId, Integer tenantId) {
        this.id = id;
        this.username = username;
        this.phone = phone;
        this.storeId = storeId;
        this.tenantId = tenantId;
    }

    @Ignore
    public Customer(String username, Integer phone){
        this.username = username;
        this.phone = phone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public Integer getTenantId() {
        return tenantId;
    }

    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", phone=" + phone +
                ", storeId=" + storeId +
                ", tenantId=" + tenantId +
                '}';
    }


}
