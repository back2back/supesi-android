package co.supesi.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiStoreProductResponse {

    @SerializedName("message")
    private String message;
    @SerializedName("storeProducts")
    List<StoreProduct> storeProducts;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<StoreProduct> getStoreProducts() {
        return storeProducts;
    }

    public void setStoreProducts(List<StoreProduct> storeProducts) {
        this.storeProducts = storeProducts;
    }


}
