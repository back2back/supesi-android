package co.supesi.model;

public interface OnProductClickListener {
    void onProductClick(Product product);
}
