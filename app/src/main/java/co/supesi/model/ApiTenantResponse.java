package co.supesi.model;

import com.google.gson.annotations.SerializedName;

public class ApiTenantResponse {

    @SerializedName("message")
    private String message;
    @SerializedName("tenant")
    private Tenant tenant;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }
}
