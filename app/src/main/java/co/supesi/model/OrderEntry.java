package co.supesi.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

@Entity
public class OrderEntry {
    @PrimaryKey
    @SerializedName("id")
    private int id;
    @SerializedName("status")
    private int status;
    @SerializedName("nice_amount")
    private int niceAmount;
    @SerializedName("orders")
    private ArrayList<Order> orders;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getNiceAmount() {
        return niceAmount;
    }

    public void setNiceAmount(int niceAmount) {
        this.niceAmount = niceAmount;
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<Order> orders) {
        this.orders = orders;
    }
}
