package co.supesi.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class StoreProduct {
    @SerializedName("store_id")
    private int storeId;
    @SerializedName("product")
    private Product product;

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
