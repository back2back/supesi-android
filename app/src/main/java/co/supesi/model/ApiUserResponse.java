package co.supesi.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiUserResponse {

    @SerializedName("users")
    private List<User> manyUsers;
    @SerializedName("message")
    private String message;
    @SerializedName("user")
    private User singleUser;
    @SerializedName("tenant")
    private Tenant tenant;
    @SerializedName("jwt")
    private String jwt;

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public List<User> getManyUsers() {
        return manyUsers;
    }

    public void setManyUsers(List<User> manyUsers) {
        this.manyUsers = manyUsers;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getSingleUser() {
        return singleUser;
    }

    public void setSingleUser(User singleUser) {
        this.singleUser = singleUser;
    }

    public Tenant getTenant() {
        return tenant;
    }

    public void setTenant(Tenant tenant) {
        this.tenant = tenant;
    }
}
