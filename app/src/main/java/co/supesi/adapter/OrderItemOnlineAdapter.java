package co.supesi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.supesi.R;
import co.supesi.model.Order;
import co.supesi.model.Product;

public class OrderItemOnlineAdapter extends RecyclerView.Adapter<OrderItemOnlineAdapter.OrderItemOnlineViewHolder>{

    LayoutInflater layoutInflater;
    Context context;
    ArrayList<Order> orders;
    public OrderItemOnlineAdapter(Context context, ArrayList<Order> orders){
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.orders = orders;

    }

    @NonNull
    @Override
    public OrderItemOnlineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.online_order_item, parent, false);
        return new OrderItemOnlineViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderItemOnlineViewHolder holder, int position) {
        Order order = orders.get(position);
        int qty = order.getQuantity();
        holder.tvQty.setText(String.valueOf(qty));

        Product product = order.getProduct();
        holder.tvProd.setText(product.getTitle());
        //holder.tvProd.setText("Jiweke");
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    class OrderItemOnlineViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_qty)
        TextView tvQty;
        @BindView(R.id.tv_product)
        TextView tvProd;
        public OrderItemOnlineViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
