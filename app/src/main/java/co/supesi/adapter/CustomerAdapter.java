package co.supesi.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.supesi.R;
import co.supesi.database.OnCustomerClickListener;
import co.supesi.model.Customer;
import co.supesi.ui.cart.ProductsCartViewModel;
import co.supesi.utility.Constants;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.CustomerViewHolder> {

    private static final String TAG = "CustomerAdapter";
    LayoutInflater layoutInflater;
    Context context;
    ArrayList<Customer> customers;
    OnCustomerClickListener listener;
    public CustomerAdapter(Context context, ArrayList<Customer> customers, OnCustomerClickListener listener){
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.customers = customers;
        this.listener = listener;
    }
    @NonNull
    @NotNull
    @Override
    public CustomerViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new CustomerViewHolder(layoutInflater.inflate(R.layout.customer_items, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull CustomerViewHolder holder, int position) {
        Customer customer = customers.get(position);
        holder.tvCustomerName.setText(customer.getUsername());
        holder.tvCustomerPhone.setText(String.valueOf(customer.getPhone()));

        holder.csLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, customer.getUsername(), Toast.LENGTH_SHORT).show();
                //add customer to order
                addCustomerOrder(customer);
            }
        });
    }

    private void addCustomerOrder(Customer customer) {
        listener.onCustomerClick(customer);
    }

    public void setCustomerItems(List<Customer> customerList){
        notifyDataSetChanged();
        this.customers = (ArrayList<Customer>) customerList;
    }

    @Override
    public int getItemCount() {
        return customers.size();
    }

    class CustomerViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_customer_name)
        TextView tvCustomerName;
        @BindView(R.id.tv_customer_phone)
        TextView tvCustomerPhone;

        @BindView(R.id.customer_layout)
        ConstraintLayout csLayout;
        public CustomerViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
