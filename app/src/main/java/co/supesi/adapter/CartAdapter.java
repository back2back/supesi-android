package co.supesi.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.supesi.BasicApp;
import co.supesi.MainActivity;
import co.supesi.R;
import co.supesi.model.Order;
import co.supesi.model.Product;
import co.supesi.ui.cart.ProductsCartViewModel;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> {

    private static final String TAG = CartAdapter.class.getSimpleName();
    ArrayList<Order> orders;
    LayoutInflater layoutInflater;
    ProductsCartViewModel productsCartViewModel;

    Context context;
    public CartAdapter(Context context, ProductsCartViewModel productsCartViewModel){
        layoutInflater = LayoutInflater.from(context);
        this.productsCartViewModel= productsCartViewModel;
        this.context = context;
    }
    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new CartViewHolder(layoutInflater.inflate(R.layout.cart_items, parent, false));
    }



    @Override
    public void onBindViewHolder(@NonNull CartViewHolder holder, int position) {

        if (orders != null){
            Order current = orders.get(position);
            holder.tvProductTitle.setText(current.getProductName());
            holder.tvPrice.setText(context.getString(R.string.zero_money, current.getPrice()));
            holder.editText.setText(String.valueOf(current.getQuantity()));


            holder.editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {

                        v.clearFocus();
                        InputMethodManager in = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        in.hideSoftInputFromWindow(v.getWindowToken(), 0);

                        current.setQuantity(Integer.valueOf(v.getText().toString()));
                        productsCartViewModel.updatedOrder(current);

                    }
                    return true;
                }
            });

            //add
            holder.btnAdd.setOnClickListener(v->{

                int currentX = current.getQuantity();
                currentX++;
                current.setQuantity(currentX);
                productsCartViewModel.updatedOrder(current);


            });

            //reduce
            holder.btnReduce.setOnClickListener(v->{
                int currentY = current.getQuantity();
                currentY--;

                current.setQuantity(currentY);
                productsCartViewModel.updatedOrder(current);
                if (currentY < 1){
                    //minimum is 1
                    //current.setQuantity(currentY);
                    //productsCartViewModel.updatedOrder(current);
                    productsCartViewModel.deleteOneOrder(current);
                    notifyDataSetChanged();

                }

            });

        }
    }

    @Override
    public int getItemCount() {
        if (orders != null){
            return orders.size();
        }
        return 0;
    }

    public void setCartItems(List<Order> orders){
        notifyDataSetChanged();
        this.orders = (ArrayList<Order>) orders;

    }



    class CartViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_product_title)
        TextView tvProductTitle;
        @BindView(R.id.btn_add)
        Button btnAdd;
        @BindView(R.id.btn_reduce)
        Button btnReduce;
        @BindView(R.id.et_qty)
        AppCompatEditText editText;
        @BindView(R.id.tv_price)
        TextView tvPrice;


        public CartViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
