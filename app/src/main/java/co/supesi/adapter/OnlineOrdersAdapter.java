package co.supesi.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.supesi.R;
import co.supesi.model.ApiReadyOrderResponse;
import co.supesi.model.Order;
import co.supesi.model.OrderEntry;
import co.supesi.network.GetData;
import co.supesi.network.RetrofitClientInstance;
import co.supesi.utility.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OnlineOrdersAdapter extends RecyclerView.Adapter<OnlineOrdersAdapter.OnlineOrdersViewHolder> {

    Context context;
    LayoutInflater layoutInflater;
    ArrayList<OrderEntry> orderEntries;
    private SharedPreferences pref;
    private String token;
    private ProgressDialog progressDialog;

    public OnlineOrdersAdapter(Context context, ArrayList<OrderEntry> orderEntries){
        layoutInflater = LayoutInflater.from(context);
        this.orderEntries = orderEntries;
        this.context = context;
        progressDialog = new ProgressDialog(context);
    }



    @NonNull
    @Override
    public OnlineOrdersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.online_orders, parent, false);
        return new OnlineOrdersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OnlineOrdersViewHolder holder, int position) {
        OrderEntry orderEntry = orderEntries.get(position);
        int total = orderEntry.getNiceAmount();
        holder.tvAmount.setText(String.valueOf(total));

        //another adapter
        ArrayList<Order> orders =  orderEntry.getOrders();
        OrderItemOnlineAdapter onlineAdapter = new OrderItemOnlineAdapter(context, orders);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        holder.rvOrders.setLayoutManager(layoutManager);
        holder.rvOrders.setAdapter(onlineAdapter);

        //ready
        holder.btnReady.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog("Submitting ...");
                readyDispatch(orderEntry.getId());
            }
        });



    }

    private void showDialog(String message) {
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();;
    }

    private void dismissDialog(){
        progressDialog.dismiss();
    }

    private void readyDispatch(int id) {
        OrderEntry orderEntry = new OrderEntry();
        orderEntry.setStatus(3);
        GetData getData = RetrofitClientInstance.getRetrofitInstance().create(GetData.class);
        Call<ApiReadyOrderResponse> responseCall = getData.dispatchOrder(loadJwt(), id, orderEntry);
        responseCall.enqueue(new Callback<ApiReadyOrderResponse>() {
            @Override
            public void onResponse(Call<ApiReadyOrderResponse> call, Response<ApiReadyOrderResponse> response) {

               if (response.body() != null){

                   dismissDialog();
                   String message = response.body().getMessage();
                   if (Boolean.parseBoolean(message)){

                       //success
                       orderEntries.clear();
                       notifyDataSetChanged();
                   }
               }
            }

            @Override
            public void onFailure(Call<ApiReadyOrderResponse> call, Throwable t) {
                    dismissDialog();
            }
        });

    }
    private String loadJwt(){
        pref = context.getSharedPreferences(Constants.getPrefJwt(), Context.MODE_PRIVATE);
        token = pref.getString("jwt",null);
        return token;
    }

    @Override
    public int getItemCount() {
        //return orderEntries.size();
        return 1;

    }

    class OnlineOrdersViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.cv_online_orders)
        CardView cardView;
        @BindView(R.id.tv_amount)
        TextView tvAmount;
        @BindView(R.id.rv_online_orders)
        RecyclerView rvOrders;
        @BindView(R.id.btn_ready_dispatch)
        Button btnReady;
        public OnlineOrdersViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
