package co.supesi.adapter;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.supesi.R;
import co.supesi.database.DataRepository;
import co.supesi.model.Product;
import co.supesi.model.ProductAndStock;
import co.supesi.model.Stock;
import co.supesi.ui.productmanage.StocksViewModel;

public class ManageProductsAdapter  extends RecyclerView.Adapter<ManageProductsAdapter.ProductsViewHolder> {

    public Context context;
    public List<ProductAndStock> products;
    LayoutInflater layoutInflater;
    private static final String TAG = "ManageProductsAdapter";
    ArrayList<Product> productArrayList;

    public List<Stock> stocks;

    public ManageProductsAdapter(Context contextl ) {

        layoutInflater = LayoutInflater.from(contextl);
        context = contextl;

    }



    @NonNull
    @Override
    public ProductsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.prod_overview, parent, false);
        ProductsViewHolder productsViewHolder = new ProductsViewHolder(view);

        productArrayList = new ArrayList<>();

        return productsViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductsViewHolder holder, int position) {

        if (products == null){
            holder.tvProduct.setText(R.string.no_product);
        }else{

            ProductAndStock current = products.get(position);
            String s = current.product.getTitle();
            String v = String.valueOf(current.stock.getCounted());

            Product product1 = current.product;
            Stock stock = current.stock;

            holder.tvProduct.setText(s);
            holder.tvStock.setText(v);

            holder.layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("amount", product1);
                    bundle.putString("stock", v);
                    bundle.putSerializable("stockPojo", stock);


                    Navigation.findNavController(view).navigate(R.id.action_nav_to_edit, bundle);
                }
            });


        }
    }

    @Override
    public int getItemCount() {
        if (products != null){
            return products.size();
        }
        return 0;
    }



    public void setProducts1(List<ProductAndStock> productAndStocks) {
        notifyDataSetChanged();
        this.products = productAndStocks;
    }


    static class ProductsViewHolder  extends RecyclerView.ViewHolder  {
        //bind views here
        @BindView(R.id.tv_product_title)
        TextView tvProduct;
        @BindView(R.id.lyt_item)
        ConstraintLayout layout;
        @BindView(R.id.tv_stock)
        TextView tvStock;
        public ProductsViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }


    }


}
