package co.supesi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.supesi.R;
import co.supesi.model.OnProductClickListener;
import co.supesi.model.Order;
import co.supesi.model.Product;
import co.supesi.ui.cart.ProductsCartViewModel;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductViewHolder> {

    public Context context;
    public List<Product> products;
    ProductsCartViewModel productsCartViewModel;

    LayoutInflater layoutInflater;
    private static final String TAG = ProductsAdapter.class.getSimpleName();
    ArrayList<Product> productArrayList;

    OnProductClickListener listener;

    public ProductsAdapter(Context context, ProductsCartViewModel productsCartViewModel, OnProductClickListener listener) {
        layoutInflater = LayoutInflater.from(context);
        this.productsCartViewModel = productsCartViewModel;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.products, parent, false);

        productArrayList = new ArrayList<>();
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {

        if (products != null){

            Product current = products.get(position);


            holder.tvProduct.setText(current.getTitle());

            holder.tvAmount.setText(context.getString(R.string.zero_money, current.getSellingPrice()));
            holder.layout.setOnClickListener(v -> {

                listener.onProductClick(current);
                // TODO: 3/5/20 investigate how to add orders from here

            });

        }else{
            holder.tvProduct.setText(R.string.no_product);
        }

    }


    @Override
    public int getItemCount() {
        if (products != null){
            return products.size();
        }
        return 0;
    }
    public void setProducts(List<Product> products){
        notifyDataSetChanged();
        this.products = products;
    }

    class ProductViewHolder extends RecyclerView.ViewHolder{
        //bind views here
        @BindView(R.id.tv_product_title)
        TextView tvProduct;
        @BindView(R.id.lyt_item)
        ConstraintLayout layout;
        @BindView(R.id.tv_amount)
        TextView tvAmount;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

    }
}
