package co.supesi;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;


import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.supesi.database.DataRepository;
import co.supesi.model.Tenant;
import co.supesi.model.User;
import co.supesi.ui.UserDataViewModel;
import co.supesi.ui.cart.ProductsCartViewModel;
import co.supesi.utility.Constants;
import co.supesi.utility.FontManager;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";
    private AppBarConfiguration mAppBarConfiguration;
    SharedPreferences preferences;
    private UserDataViewModel userDataViewModel;
    ProductsCartViewModel productsCartViewModel;
    private DataRepository dataRepository;

    private Unbinder unbinder;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        //Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //DrawerLayout drawer = findViewById(R.id.drawer_layout);
        //NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        launchChat(navController);

        userDataViewModel = new ViewModelProvider(this).get(UserDataViewModel.class);

        View view = navigationView.getHeaderView(0);
        TextView tvTenantNav = view.findViewById(R.id.tv_nav_tenant);
        TextView tvEmailNav = view.findViewById(R.id.tv_nav_email);
        //check database if user is logged in
        check(tvEmailNav, tvTenantNav);


        //TextView tvFab = view.findViewById(R.id.tv_fab);
        //Typeface typeface = FontManager.get(BasicApp.getContext(),"fa-regular-400.ttf");
        //tvFab.setTypeface(typeface);

        //firebaseToken();
    }

    private void launchChat(NavController navController) {
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                if (destination.getId() == R.id.nav_chat){
                    Toast.makeText(MainActivity.this, "Foobar", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    // TODO: 4/6/20 fetch data by  id. Ideally should be one entry in each table. Users and tenants
    private void check(TextView email, TextView tenantName){
        if(userDataViewModel.getUser() != null){

            userDataViewModel.getUser().observe(this, new Observer<List<User>>() {
                @Override
                public void onChanged(List<User> users) {

                    if(users.size() > 0){
                        email.setText(users.get(0).getEmail());
                    }

                }
            });
            userDataViewModel.getTenant().observe(this, new Observer<List<Tenant>>() {
                @Override
                public void onChanged(List<Tenant> tenants) {
                    if (tenants.size() > 0){
                        tenantName.setText(tenants.get(0).getName());
                    }
                }
            });

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_settings:
                //prefClear();
                resetApp();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void resetApp(){
        //delete some tables
        //dataRepository = new DataRepository(getApplication());
        dataRepository = BasicApp.getContext().getRepository();
        dataRepository.nukeTables();

        //prefClear();
        //System.exit(0);

    }
    private void prefClear(){

        preferences = getApplication().getSharedPreferences(Constants.getPrefJwt(), Context.MODE_PRIVATE);
        preferences.edit().clear().apply();

    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }





}
