package co.supesi.utility;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import co.supesi.BasicApp;
import co.supesi.database.DataRepository;
import co.supesi.model.ApiUserResponse;
import co.supesi.model.Tenant;
import co.supesi.model.User;

public class SaveUser extends AndroidViewModel {

    private  DataRepository dataRepository;

    public SaveUser(@NonNull Application application) {
        super(application);

        dataRepository = BasicApp.getContext().getRepository();
    }
    public void saveUserData(ApiUserResponse apiUserResponse) {
        User user = apiUserResponse.getSingleUser();
        Tenant tenant = apiUserResponse.getTenant();
        dataRepository.insertUser(user, tenant);
    }


}
