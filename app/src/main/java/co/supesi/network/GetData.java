package co.supesi.network;

import java.util.ArrayList;

import butterknife.BindView;
import co.supesi.model.ApiCustomerResponse;
import co.supesi.model.ApiOrderResponse;
import co.supesi.model.ApiStockRetake;
import co.supesi.model.ApiStoreProductResponse;
import co.supesi.model.ApiReadyOrderResponse;
import co.supesi.model.ApiReportResponse;
import co.supesi.model.ApiTenantResponse;
import co.supesi.model.ApiUserResponse;
import co.supesi.model.Customer;
import co.supesi.model.Order;
import co.supesi.model.OrderEntry;
import co.supesi.model.Product;
import co.supesi.model.StockRetake;
import co.supesi.model.Tenant;
import co.supesi.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface GetData {

    @GET("store-products.json")
    Call<ApiStoreProductResponse> getAllProducts(@Header("Authorization") String jwt);

    @GET("order-entries/interval/1.json")
    Call<ApiReportResponse> getTodayReport(@Header("Authorization") String jwt);

    @GET("order-entries/online.json")
    Call<ApiReadyOrderResponse> getReadyOrders(@Header("Authorization") String jwt);

    @PUT("order-entries/{id}.json")
    Call<ApiReadyOrderResponse> dispatchOrder(@Header("Authorization") String jwt, @Path("id") int id, @Body OrderEntry orderEntry);

    @POST("products.json")
    Call<ApiStoreProductResponse> addProduct(@Body Product product, @Header("Authorization") String jwt);

    @PUT("products/{id}.json")
    Call<ApiStoreProductResponse> editProduct(@Header("Authorization") String jwt, @Path("id") int id, @Body Product product);

    @POST("users/login.json")
    Call<ApiUserResponse> login(@Body User user);

    @POST("users.json")
    Call<ApiUserResponse> addUser(@Body User user);

    @POST("orders.json")
    Call<ApiOrderResponse> addOrders(@Header("Authorization") String jwt, @Body ArrayList<Order> orders);

    @POST("search.json")
    Call<ApiTenantResponse> searchTenant(@Body Tenant tenant);

    @GET("users/profile.json")
    Call<ApiUserResponse> getProfile(@Header("Authorization") String jwt);

    @POST("stock-retakes.json")
    Call<ApiStockRetake> addStockRetake(@Header("Authorization") String jwt, @Body StockRetake stockRetake);

    @GET("customers.json")
    Call<ApiCustomerResponse> getCustomers(@Header("Authorization") String jwt);

    @POST("customers.json")
    Call<ApiCustomerResponse> addCustomer(@Header("Authorization") String jwt, @Body Customer customer);

}
